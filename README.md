Pre-commit
=========

Role that installs Pre-commit

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Pre-commit on targeted machine:

    - hosts: servers
      roles:
         - precommit

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
